#include <iostream>
#include <ctime>
#include "MatrixBlas.h"
#include "Matrix.h"
#include "MatrixHand.h"


int main() {
    //srand(time(NULL));

    int N = 2048;     //matrix size
    int M = 0;        //count repeat

    std::vector<float> arr;
    arr.reserve(N * N);
    for (int i = 0, size = N * N; i < size; ++i) {
        arr.push_back(i);
    }

    Matrix matrix = Matrix(N, arr);
    MatrixHand matrixHand = MatrixHand(N, arr);
    MatrixBlas matrixBlas = MatrixBlas(N, arr);

    int clockS, clockF;

    clockS = time(NULL);
    matrix = matrix.reverse(M);
    clockF = time(NULL);
    std::cout << "Matrix: " << (clockF - clockS)  << std::endl;

    clockS = time(NULL);
    matrixHand = matrixHand.reverse(M);
    clockF = time(NULL);
    std::cout << "MatrixHand: " << (clockF - clockS)  << std::endl;

    clockS = time(NULL);
    matrixBlas = matrixBlas.reverse(M);
    clockF = time(NULL);
    std::cout << "MatrixBlas: " << (clockF - clockS)  << std::endl;

    return 0;
}