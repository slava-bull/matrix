//
// Created by Slava on 21.11.2018.
//

#ifndef LAB7_MATRIX_H
#define LAB7_MATRIX_H

#include <iostream>
#include <vector>
#include <cmath>
#include <cstring>

class Matrix {
protected:
    float *matrix_ = NULL;
    int size_ = 0;
public:
    explicit Matrix(int size, float val = 1);

    Matrix(int size, const std::vector<float> &val);

    Matrix(const Matrix &other);

    ~Matrix();

    void print() const;

    void set(int h, int w, float val);

    float get(int h, int w) const;

    Matrix reverse(int count) const;

    Matrix transpose() const;

    Matrix operator/(float d) const;

    Matrix &operator=(const Matrix &other);

    float maxColumn() const;

    float maxLine() const;

    Matrix operator*(float d) const;

    Matrix operator*(const Matrix &other) const;

    Matrix operator+(const Matrix &other) const;

    Matrix operator-(const Matrix &other) const;
};


#endif //LAB7_MATRIX_H
