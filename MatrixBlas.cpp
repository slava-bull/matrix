//
// Created by Slava on 21.11.2018.
//

#include "MatrixBlas.h"
#include <cblas.h>

MatrixBlas::MatrixBlas(int size, float val) : size_(size) {
    matrix_ = new float[size_ * size_]();

    for (int i = 0; i < size_; ++i) {
        set(i, i, val);
    }
}

MatrixBlas::MatrixBlas(int size, const std::vector<float> &val) : size_(size) {
    if (val.size() < size_ * size_) {
        throw std::runtime_error("Need more values");
    }

    matrix_ = new float[size_ * size_]();

    for (int i = 0; i < size_; ++i) {
        for (int j = 0; j < size_; ++j) {
            set(i, j, val[i * size_ + j]);
        }
    }
}

MatrixBlas::MatrixBlas(const MatrixBlas &other) : size_(other.size_) {
    matrix_ = new float[size_ * size_]();

    memcpy(matrix_, other.matrix_, size_ * size_ * sizeof(float));
}

MatrixBlas::~MatrixBlas() {
    delete[] matrix_;
    matrix_ = NULL;
}

void MatrixBlas::print() const {
    for (int i = 0; i < size_; ++i) {
        for (int j = 0; j < size_; ++j) {
            std::cout << get(i, j) << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void MatrixBlas::set(int h, int w, float val) {
    matrix_[h * size_ + w] = val;
}

float MatrixBlas::get(int h, int w) const {
    return matrix_[h * size_ + w];
}

MatrixBlas MatrixBlas::transpose() const {
    MatrixBlas result(size_);
    for (int i = 0; i < size_; ++i) {
        for (int j = 0; j < size_; ++j) {
            result.set(j, i, get(i, j));
        }
    }

    return result;
}

MatrixBlas MatrixBlas::reverse(int count) const {
    MatrixBlas result(size_);
    MatrixBlas I(size_);
    MatrixBlas B = this->transpose() / (maxColumn() * maxLine());
    MatrixBlas R = I - B * (*this);
    const MatrixBlas constR = R;

    for (int i = 1; i < count; ++i) {
        result = result + R;
        R = R * constR;
    }

    return result * B;
}

MatrixBlas MatrixBlas::operator/(float d) const {
    return (*this) * (1 / d);
}

MatrixBlas &MatrixBlas::operator=(const MatrixBlas &other) {
    if (this != &other) {
        if (size_ != other.size_) {
            delete[] matrix_;
            size_ = other.size_;
            matrix_ = new float[size_ * size_];
        }
        memcpy(matrix_, other.matrix_, size_ * size_ * sizeof(float));
    }

    return *this;
}

////////////////////////////////////////////////////

float MatrixBlas::maxColumn() const {
    float max = 0;
    for (int i = 0; i < size_; ++i) {
        float tmp = cblas_sasum(size_, matrix_ + i, size_);//N matrix incMatrix
        if (max < tmp) {
            max = tmp;
        }
    }

    return max;
}

float MatrixBlas::maxLine() const {
    float max = 0;
    for (int i = 0; i < size_; ++i) {
        float tmp = cblas_sasum(size_, matrix_ + i * size_, 1);//N matrix incMatrix
        if (max < tmp) {
            max = tmp;
        }
    }

    return max;
}

MatrixBlas MatrixBlas::operator*(float d) const {
    MatrixBlas result = *this;
    cblas_sscal(size_ * size_, d, result.matrix_, 1);//N value matrix incMatrix
    return result;
}

MatrixBlas MatrixBlas::operator*(const MatrixBlas &other) const {
    if (size_ != other.size_) {
        throw std::runtime_error("MatrixBlas have different size");
    }

    MatrixBlas result(size_);
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, size_, size_, size_, 1, matrix_, size_, other.matrix_,
                size_, 0.f, result.matrix_, size_);

    /*
     void cblas_sgemm(const enum CBLAS_ORDER Order,
   const enum CBLAS_TRANSPOSE TransA, const enum CBLAS_TRANSPOSE TransB,
   const int M, const int N,
   const int K, const float alpha, const float *A,
   const int lda, const float *B, const int ldb,
   const float beta, float *C, const int ldc);
где:        Order - определяет порядок следования элементов:
                        CblasRowMajor - матрицы хранятся по строкам (стандартно в C),
                        CblasColMajor - матрицы хранятся по столбцам;
            TransA, TransB - определяет предварительные операции над матрицами A и B:
                        CblasNoTrans - ничего не делать,
                        CblasTrans - транспонировать,
                        CblasConjTrans - вычислить сопряженную матрицу;
            M, N, K          - размеры матриц;
            alpha, beta       - коэффициенты;
            lda, ldb, ldc      - число элементов в ведущей размерности матрицы (строке или столбце). Для массивов языка Си - число элементов в строке:
                        lda = K
                        ldb = N
                        ldc = N
      */

    return result;
}

MatrixBlas MatrixBlas::operator+(const MatrixBlas &other) const {
    if (size_ != other.size_) {
        throw std::runtime_error("MatrixBlas have different size");
    }

    MatrixBlas result(size_);
    for (int i = 0; i < size_; ++i) {
        for (int j = 0; j < size_; ++j) {
            result.set(i, j, get(i, j) + other.get(i, j));
        }
    }

    return result;
}

MatrixBlas MatrixBlas::operator-(const MatrixBlas &other) const {
    if (size_ != other.size_) {
        throw std::runtime_error("MatrixBlas have different size");
    }

    MatrixBlas result(size_);
    for (unsigned i = 0; i < size_; ++i) {
        for (unsigned j = 0; j < size_; ++j) {
            result.set(i, j, get(i, j) - other.get(i, j));
        }
    }

    return result;
}
