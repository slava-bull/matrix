//
// Created by Slava on 21.11.2018.
//

#include "MatrixHand.h"
#include <xmmintrin.h>
#include <stdexcept>
#include <iostream>

MatrixHand::MatrixHand(int size, float val) : size_(size) {
    matrix_ = new float[size_ * size_]();

    for (int i = 0; i < size_; ++i) {
        set(i, i, val);
    }
}

MatrixHand::MatrixHand(int size, const std::vector<float> &val) : size_(size) {
    if (val.size() < size_ * size_) {
        throw std::runtime_error("Need more values");
    }

    matrix_ = new float[size_ * size_]();

    for (int i = 0; i < size_; ++i) {
        for (int j = 0; j < size_; ++j) {
            set(i, j, val[i * size_ + j]);
        }
    }
}

MatrixHand::MatrixHand(const MatrixHand &other) : size_(other.size_) {
    matrix_ = new float[size_ * size_]();

    memcpy(matrix_, other.matrix_, size_ * size_ * sizeof(float));
}

MatrixHand::~MatrixHand() {
    delete[] matrix_;
    matrix_ = NULL;
}

void MatrixHand::print() const {
    for(int i = 0; i < size_; ++i) {
        for(int j = 0; j < size_; ++j) {
            std::cout << get(i, j) << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void MatrixHand::set(int h, int w, float val) {
    matrix_[h * size_ + w] = val;
}

float MatrixHand::get(int h, int w) const {
    return matrix_[h * size_ + w];
}

MatrixHand MatrixHand::transpose() const {
    MatrixHand result(size_);
    for(int i = 0; i < size_; ++i) {
        for(int j = 0; j < size_; ++j) {
            result.set(j, i, get(i, j));
        }
    }

    return result;
}

MatrixHand MatrixHand::reverse(int count) const {
    MatrixHand result(size_);
    MatrixHand I(size_);
    MatrixHand B = this->transpose() / (maxColumn() * maxLine());
    MatrixHand R = I - B*(*this);
    const MatrixHand constR = R;

    for(int i = 1; i < count; ++i) {
        result = result + R;
        R = R * constR;
    }

    return result * B;
}

MatrixHand MatrixHand::operator/(float d) const {
    return (*this) * (1/d);
}

MatrixHand &MatrixHand::operator=(const MatrixHand &other) {
    if(this != &other) {
        if(size_ != other.size_) {
            delete[] matrix_;
            size_ = other.size_;
            matrix_ = new float[size_*size_];
        }
        memcpy(matrix_, other.matrix_, size_ * size_ * sizeof(float));
    }

    return *this;
}

////////////////////////////////////////////

float MatrixHand::maxColumn() const {
    __m128 *d = (__m128*)matrix_;
    __m128 tmp;
    float sum = 0;
    float max = 0;
    for(int i = 0; i < size_; ++i) {
        tmp = _mm_setzero_ps();//=0
        for(int j = 0, size = size_ / 4; j < size; ++j) {//128/4=32
            tmp = _mm_add_ps(tmp, d[j]);
        }
        _mm_store_ss(&sum, tmp);//sum=tmp
        if(max < sum) {
            max = sum;
        }
    }

    return max;
}

float MatrixHand::maxLine() const {
    __m128 *d = (__m128*)matrix_;
    __m128 tmp;
    float sum = 0;
    float max = 0;
    for(int i = 0; i < size_; ++i) {
        tmp = _mm_setzero_ps();
        for(int j = 0, size = size_ / 4; j < size; ++j) {
            tmp = _mm_add_ps(tmp, d[j]);
        }
        _mm_store_ss(&sum, tmp);
        if(max < sum) {
            max = sum;
        }
    }

    return max;
}

MatrixHand MatrixHand::operator*(float d) const {
    MatrixHand result(size_);
    __m128 *data = (__m128*)result.matrix_;
    __m128 tmp = {d, d, d ,d};
    for(unsigned i = 0; i < size_; ++i) {
        for(unsigned j = 0, size = size_ / 4; j < size; ++j) {
            data[j] = _mm_mul_ps(data[j], tmp);
        }
    }

    return result;
}

MatrixHand MatrixHand::operator*(const MatrixHand &other) const {
    if(size_ != other.size_) {
        throw std::runtime_error("MatrixHand have different size");
    }

    MatrixHand result(size_);
    __m128 *m1 = (__m128*)matrix_;
    __m128 *m2 = (__m128*)other.matrix_;
    __m128 tmp;
    for(int i = 0; i < size_; ++i) {
        for(int j = 0; j < size_; ++j) {
            tmp = _mm_setzero_ps();
            for(int k = 0, size = size_ / 4; k < size; ++k) {
                tmp = _mm_add_ps(tmp, _mm_mul_ps(m1[k], m2[k]));
            }
            float sum;
            _mm_store_ss(&sum, tmp);
            result.set(i, j, sum);
        }
    }

    return result;
}

MatrixHand MatrixHand::operator+(const MatrixHand &other) const {
    if(size_ != other.size_) {
        throw std::runtime_error("MatrixHand have different size");
    }

    MatrixHand result(size_);
    __m128 *m1 = (__m128*)matrix_;
    __m128 *m2 = (__m128*)other.matrix_;
    __m128 *m3 = (__m128*)result.matrix_;
    for(int i = 0; i < size_; ++i) {
        for(int j = 0, size = size_ / 4; j < size; ++j) {
            m3[j] = _mm_add_ps(m1[j], m2[j]);
        }
    }

    return result;
}

MatrixHand MatrixHand::operator-(const MatrixHand &other) const {
    if(size_ != other.size_) {
        throw std::runtime_error("MatrixHand have different size");
    }

    MatrixHand result(size_);
    __m128 *m1 = (__m128*)matrix_;
    __m128 *m2 = (__m128*)other.matrix_;
    __m128 *m3 = (__m128*)result.matrix_;
    for(unsigned i = 0; i < size_; ++i) {
        for(unsigned j = 0, size = size_ / 4; j < size; ++j) {
            m3[j] = _mm_sub_ps(m1[j], m2[j]);
        }
    }

    return result;
}


