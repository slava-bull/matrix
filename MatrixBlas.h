//
// Created by Slava on 21.11.2018.
//

#ifndef LAB7_MATRIXBLAS_H
#define LAB7_MATRIXBLAS_H

#include <iostream>
#include <vector>
#include <cstring>

class MatrixBlas {
protected:
    float *matrix_ = NULL;
    int size_ = 0;
public:
    explicit MatrixBlas(int size, float val = 1);

    MatrixBlas(int size, const std::vector<float> &val);

    MatrixBlas(const MatrixBlas &other);

    ~MatrixBlas();

    void print() const;

    void set(int h, int w, float val);

    float get(int h, int w) const;

    MatrixBlas reverse(int count) const;

    MatrixBlas transpose() const;

    MatrixBlas operator/(float d) const;

    MatrixBlas &operator=(const MatrixBlas &other);

    float maxColumn() const;

    float maxLine() const;

    MatrixBlas operator*(float d) const;

    MatrixBlas operator*(const MatrixBlas &other) const;

    MatrixBlas operator+(const MatrixBlas &other) const;

    MatrixBlas operator-(const MatrixBlas &other) const;
};


#endif //LAB7_MATRIXBLAS_H
