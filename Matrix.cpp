//
// Created by Slava on 21.11.2018.
//

#include "Matrix.h"

Matrix::Matrix(int size, float val) : size_(size) {
    matrix_ = new float[size_ * size_]();

    for (int i = 0; i < size_; ++i) {
        set(i, i, val);
    }
}

Matrix::Matrix(int size, const std::vector<float> &val) : size_(size) {
    if (val.size() < size_ * size_) {
        throw std::runtime_error("Need more values");
    }

    matrix_ = new float[size_ * size_]();

    for (int i = 0; i < size_; ++i) {
        for (int j = 0; j < size_; ++j) {
            set(i, j, val[i * size_ + j]);
        }
    }
}

Matrix::Matrix(const Matrix &other) : size_(other.size_) {
    matrix_ = new float[size_ * size_]();

    memcpy(matrix_, other.matrix_, size_ * size_ * sizeof(float));
}

Matrix::~Matrix() {
    delete[] matrix_;
    matrix_ = NULL;
}

void Matrix::print() const {
    for(int i = 0; i < size_; ++i) {
        for(int j = 0; j < size_; ++j) {
            std::cout << get(i, j) << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Matrix::set(int h, int w, float val) {
    matrix_[h * size_ + w] = val;
}

float Matrix::get(int h, int w) const {
    return matrix_[h * size_ + w];
}

float Matrix::maxColumn() const {
    float max = 0;
    for(int i = 0; i < size_; ++i) {
        float tmp = 0;
        for(int j = 0; j < size_; ++j) {
            tmp += fabs(get(j, i));
        }
        if(max < tmp) {
            max = tmp;
        }
    }

    return max;
}

float Matrix::maxLine() const {
    float max = 0;
    for(int i = 0; i < size_; ++i) {
        float tmp = 0;
        for(int j = 0; j < size_; ++j) {
            tmp += fabs(get(i, j));
        }
        if(max < tmp) {
            max = tmp;
        }
    }

    return max;
}

Matrix Matrix::transpose() const {
    Matrix result(size_);
    for(int i = 0; i < size_; ++i) {
        for(int j = 0; j < size_; ++j) {
            result.set(j, i, get(i, j));
        }
    }

    return result;
}

Matrix Matrix::reverse(int count) const {
    Matrix result(size_);
    Matrix I(size_);
    Matrix B = this->transpose() / (maxColumn() * maxLine());
    Matrix R = I - B*(*this);
    const Matrix constR = R;

    for(int i = 1; i < count; ++i) {
        result = result + R;
        R = R * constR;
    }

    return result * B;
}

Matrix Matrix::operator/(float d) const {
    return (*this) * (1/d);
}

Matrix Matrix::operator*(float d) const {
    Matrix result(size_);
    for(unsigned i = 0; i < size_; ++i) {
        for(unsigned j = 0; j < size_; ++j) {
            result.set(i, j, get(i, j) * d);
        }
    }

    return result;
}

Matrix Matrix::operator*(const Matrix &other) const {
    if(size_ != other.size_) {
        throw std::runtime_error("Matrix have different size");
    }

    Matrix result(size_);
    for(unsigned i = 0; i < size_; ++i) {
        for(unsigned j = 0; j < size_; ++j) {
            float tmp = 0;
            for(unsigned k = 0; k < size_; ++k) {
                tmp += get(i, k) * other.get(k, j);
            }
            result.set(i, j, tmp);
        }
    }

    return result;
}

Matrix Matrix::operator+(const Matrix &other) const {
    if(size_ != other.size_) {
        throw std::runtime_error("Matrix have different size");
    }

    Matrix result(size_);
    for(unsigned i = 0; i < size_; ++i) {
        for(unsigned j = 0; j < size_; ++j) {
            result.set(i, j, get(i, j) + other.get(i, j));
        }
    }

    return result;
}

Matrix Matrix::operator-(const Matrix &other) const {
    if(size_ != other.size_) {
        throw std::runtime_error("Matrix have different size");
    }

    Matrix result(size_);
    for(unsigned i = 0; i < size_; ++i) {
        for(unsigned j = 0; j < size_; ++j) {
            result.set(i, j, get(i, j) - other.get(i, j));
        }
    }

    return result;
}

Matrix &Matrix::operator=(const Matrix &other) {
    if(this != &other) {
        if(size_ != other.size_) {
            delete[] matrix_;
            size_ = other.size_;
            matrix_ = new float[size_*size_];
        }
        memcpy(matrix_, other.matrix_, size_ * size_ * sizeof(float));
    }

    return *this;
}










