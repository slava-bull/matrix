//
// Created by Slava on 21.11.2018.
//

#ifndef LAB7_MATRIXHAND_H
#define LAB7_MATRIXHAND_H

#include <vector>
#include <cstring>

class MatrixHand {
protected:
    float *matrix_ = NULL;
    int size_ = 0;
public:
    explicit MatrixHand(int size, float val = 1);

    MatrixHand(int size, const std::vector<float> &val);

    MatrixHand(const MatrixHand &other);

    ~MatrixHand();

    void print() const;

    void set(int h, int w, float val);

    float get(int h, int w) const;

    MatrixHand reverse(int count) const;

    MatrixHand transpose() const;

    MatrixHand operator/(float d) const;

    MatrixHand &operator=(const MatrixHand &other);

    float maxColumn() const;

    float maxLine() const;

    MatrixHand operator*(float d) const;

    MatrixHand operator*(const MatrixHand &other) const;

    MatrixHand operator+(const MatrixHand &other) const;

    MatrixHand operator-(const MatrixHand &other) const;
};


#endif //LAB7_MATRIXHAND_H
